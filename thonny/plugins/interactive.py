import os
import os.path
import json
import zipfile
import sys
import tkinter as tk
import tkinter.font as font
import tkinterhtml
from thonny.ui_utils import select_sequence
from thonny.globals import get_workbench, get_runner
from thonny.common import ToplevelCommand
from tkinter.filedialog import askopenfilename
from tkinter import ttk

CALM_WHITE = '#fdfdfd'
COMMAND_NAME = "InteractiveTest"
_dialog_filetype = [('zip files', '.zip')]


class Interactive():
    def __init__(self):

        get_workbench().add_command("open_new_course", "file", "Open a new course...",
                                    self.open_new_course,
                                    default_sequence=select_sequence("<Control-t>", "<Command-t>"),
                                    group=20,
                                    image_filename="file.open_file.gif",
                                    include_in_toolbar=False)

        get_workbench().add_command("open_existing_course", "file", "Open an existing course...",
                                    self.open_existing_course,
                                    default_sequence=select_sequence("<Control-Shift-T>", "<Command-Shift-T>"),
                                    group=20,
                                    image_filename="file.open_file.gif",
                                    include_in_toolbar=False)

        get_workbench().add_command("close_course", "file", "Close this course",
                                    self.close_course,
                                    default_sequence=select_sequence("<Control-q>", "<Command-q>"),
                                    group=20,
                                    include_in_toolbar=False)

        # change the icon
        get_workbench().add_command("check task", "run", "Check task",
                                    self.check_task,
                                    default_sequence=select_sequence("<Control-F10>", "<Control-F10>"),
                                    tester=lambda: get_workbench().get_option(
                                        "interactive.last_course_folder") is not None,
                                    group=80,
                                    image_filename=get_full_path("run.check_task.png"),
                                    include_in_toolbar="True")

        get_workbench().add_command("Show hint", "run", "Show hint",
                                    self.show_hint,
                                    default_sequence=select_sequence("<Control-h>", "<Command-h>"),
                                    tester=lambda: get_workbench().get_option(
                                        "interactive.last_course_folder") is not None,
                                    group=80,
                                    image_filename=get_full_path("run.show_hint.png"),
                                    include_in_toolbar="True")

        self.course_filename = None  # remembers course name
        self.opened_filename = None  # remembers what file is opened at the moment
        self.open_inital_folder()

        # when notebook tab is changed, it changes the self.opened_filename
        get_workbench().get_editor_notebook().bind("<<NotebookTabChanged>>", self.set_opened_filename, True)

    # sets self.opened_filename
    def set_opened_filename(self, event):
        editor = get_workbench().get_editor_notebook().get_current_editor()
        if editor:
            if (editor.get_filename()):
                self.opened_filename = editor.get_filename()

    # opens last opened folder
    def open_inital_folder(self):
        course_path = get_workbench().get_option("interactive.last_course_folder")

        if (course_path):
            if (os.path.isdir(course_path)):
                self.course_filename = os.path.basename(course_path)

                get_workbench().show_view("Task_Browser")
                get_workbench().show_view("TaskDescriptionFrame")
                get_workbench().show_view("CourseFrame")
                get_workbench().get_view("Task_Browser").open_course(course_path, self.course_filename)

        else:
            get_workbench().hide_view("Task_Browser")
            get_workbench().hide_view("TaskDescriptionFrame")
            get_workbench().hide_view("CourseFrame")

    def open_new_course(self):
        path = askopenfilename(
            filetypes=_dialog_filetype,
            initialdir=get_workbench().get_option("run.working_directory")
        )
        if path:
            self.course_filename = os.path.basename(path).split('.')[0]

            course_path = os.path.expanduser(os.path.join("~", ".thonny",
                                                          "interactive_courses", self.course_filename))
            course_backup_path = os.path.expanduser(os.path.join("~", ".thonny",
                                                                 "interactive_courses_backup", self.course_filename))

            with zipfile.ZipFile(path, 'r') as zip:
                zip.extractall(course_path)
                zip.extractall(course_backup_path)

            get_workbench().add_view("Task_Browser")
            get_workbench().add_view("TaskDescriptionFrame")
            get_workbench().add_view("CourseFrame")
            # get_workbench().get_view("Task_Browser").open_course(course_path, self.course_filename)
            get_workbench().set_option("interactive.last_course_folder", course_path)

    def open_existing_course(self):
        courses_path = os.path.expanduser(os.path.join("~", ".thonny",
                                                       "interactive_courses"))
        SelectCourseFrame(get_workbench(), os.listdir(courses_path))

    def close_course(self):

        get_workbench().set_option("interactive.last_course_folder", None)
        get_workbench().hide_view("Task_Browser")
        get_workbench().hide_view("TaskDescriptionFrame")
        get_workbench().hide_view("CourseFrame")

    # checks if user has solved the task correctly
    def check_task(self):
        # saves file
        get_workbench().get_editor_notebook().save_all_named_editors()
        # gets opened file all tags
        tags = get_workbench().get_current_editor().get_text_widget().tag_names()
        first_tag = self.opened_filename + "_0"

        # checks if file contains placeholders, if not then doesn't check it
        if first_tag in tags:
            placeholder_path = self.opened_filename[:-3] + "_windows"  # removes '.py'

            # check if the path exists
            file = open(placeholder_path, "w", encoding="UTF-8")

            # finds all the tags in file and writes them to placeholder file
            i = 0
            notDone = True
            while notDone:
                tag_name = self.opened_filename + "_" + str(i)
                if tag_name in tags:
                    tag_ranges = get_workbench().get_current_editor().get_text_widget().tag_ranges(tag_name)
                    placeholder_text = "#educational_plugin_window = " \
                                       + get_workbench().get_current_editor().get_text_widget().get(tag_ranges[0],
                                                                                                    tag_ranges[1]) \
                                       + '\n'
                    file.write(placeholder_text)
                    i = i + 1
                else:
                    notDone = False
            file.close()

            run_test_menu_available()
            run_test_menu_command()

    def show_hint(self):

        hints = get_workbench().get_view('Task_Browser').hint_list
        InteractiveDialog(get_workbench(), "Interactive Task Hints", hints)


class TaskFileBrowser(ttk.Treeview):
    def __init__(self, master):
        ttk.Treeview.__init__(self, master)

        self.grid()
        self.heading("#0", text="Tasks")

        wb = get_workbench()
        self.folder_icon = wb.get_image("folder.gif")
        self.python_file_icon = wb.get_image("python_file.gif")
        self.text_file_icon = wb.get_image("text_file.gif")

        self.path = None  # remembers course path
        self.opened_filename = None  # remembers what file is opened at the moment
        self.done_task_list = []  # keeps track what tasks are done
        self.all_task = 0  # remembers how many tasks course has
        self.hint_list = ""  # stores all hints
        self.course_name = None  # remembers course name

        self.bind("<<TreeviewSelect>>", self.open_task)

        wb.bind("Save", self.save_placeholders, True)
        wb.bind("SaveAs", self.save_placeholders, True)
        wb.get_editor_notebook().bind("<<NotebookTabChanged>>", self.set_opened_filename, True)

        self.tag_configure('done', foreground='green')  # tag for done tasks

    # sets self.opened_filename
    def set_opened_filename(self, event):
        editor = get_workbench().get_editor_notebook().get_current_editor()
        if editor:
            if (editor.get_filename()):
                self.opened_filename = editor.get_filename()

    def open_course(self, path, course_name):
        # if there is a tree already, deletes it first
        if len(self.get_children()) != 0:
            self.delete(*self.get_children())

        self.path = path
        self.course_name = course_name

        # resets all_task and done_task_list
        self.all_task = 0
        self.done_task_list = []

        tasks_done_text = ""

        with open(path + '/course.json', encoding="utf-8") as data_file:
            data = json.load(data_file, encoding="utf-8")

        # find if task is already done
        if os.path.isfile(path + "/tasks_done"):
            tasks_done_file = open(path + "/tasks_done", "r", encoding="utf-8")
            tasks_done_text = tasks_done_file.read()
            newfile = False
        else:
            tasks_done_file = open(path + "/tasks_done", "w+", encoding="utf-8")
            newfile = True

        root_id = self.insert("",
                              "end",
                              text=data["name"],
                              image=self.folder_icon)

        lesson_nr = 1
        for lesson in data["lessons"]:

            if "name" in lesson:  # depending on what the key is in json
                lesson_name = lesson["name"]
            else:
                lesson_name = lesson["title"]

            lesson_path = "lesson" + str(lesson_nr)
            lesson_id = self.insert(root_id,
                                    "end",
                                    text=lesson_name,
                                    image=self.folder_icon)

            task_nr = 1
            for task in lesson["task_list"]:
                task_name = task["name"]
                task_path = "task" + str(task_nr)
                value = 'false'

                # if new file, writes all the tasks into tasks_done file
                # otherwise checks if task is done or not
                if newfile:
                    tasks_done_file.write(lesson_name + "_" + task_name + "_false\n")
                else:
                    for line in tasks_done_text.split("\n"):
                        if line.startswith(lesson_name + "_" + task_name):
                            value = line.split("_")[-1]

                            if value == 'true':
                                self.done_task_list.append(lesson_name + "_" + task_name + "\n")

                task_id = self.insert(lesson_id,
                                      "end",
                                      text=task_name,
                                      image=self.folder_icon,
                                      values=value)

                self.all_task = self.all_task + 1

                for file in task["task_files"]:
                    file_path = path + "/" + lesson_path + "/" + task_path + "/" + file

                if file[-3:] == ".py":
                    entry_id = self.insert(task_id,
                                           "end",
                                           text=file,
                                           image=self.python_file_icon,
                                           values=[file_path, lesson_name, task_name, file])
                else:
                    entry_id = self.insert(task_id,
                                           "end",
                                           text=file,
                                           image=self.text_file_icon,
                                           values=[file_path, lesson_name, task_name, file])

                task_nr = task_nr + 1
            lesson_nr = lesson_nr + 1
        tasks_done_file.close()

        get_workbench().get_view("CourseFrame").fill_progress(self.all_task, self.done_task_list)
        self.refresh_tree_done()

    # adds tags to done tasks
    def refresh_tree_done(self):
        for course in self.get_children():
            lessons_done = 0
            all_lessons = self.get_children(course)

            for lesson in self.get_children(course):
                tasks_done = 0
                all_tasks = len(self.get_children(lesson))

                for task in self.get_children(lesson):
                    if self.item(task)['values'][0] == 'true':
                        self.item(task, tags=('done',))
                        tasks_done = tasks_done + 1

                if tasks_done == all_tasks:
                    self.item(lesson, tags=('done',))
                    lessons_done = lessons_done + 1

            if lessons_done == all_lessons:
                self.item(course, tags=('done',))

        self.tag_configure('done', foreground='green')

    def open_task(self, event):
        node_id = self.focus()
        if (self.item(node_id)['values']):
            path = self.item(node_id)['values'][0]
            if (os.path.isfile(path)):
                get_workbench().get_editor_notebook().show_file(path)
                self.opened_filename = path
                self.get_placeholders(node_id, path)

    def get_placeholders(self, node_id, path):
        # empties hint_list
        self.hint_list = ""

        # get data
        lesson_name = self.item(node_id)['values'][1]
        task_name = self.item(node_id)['values'][2]
        file_name = self.item(node_id)['values'][3]

        # checks if task is done
        tasks_done_file = open(self.path + "/tasks_done", "r", encoding="utf-8")
        tasks_done_text = tasks_done_file.read()
        tasks_done_file.close()

        done = False

        for line in tasks_done_text.split("\n"):
            if line.startswith(lesson_name + "_" + task_name):
                value = line.split("_")[-1]
                if value == 'true':
                    done = True

        # finds placeholders
        if self.path:
            with open(self.path + '/course.json', encoding="utf-8") as data_file:
                data = json.load(data_file, encoding="utf-8")
                for lesson in data["lessons"]:

                    # depending on what the key is in json
                    if "name" in lesson:
                        lesson_json_name = lesson["name"]
                    else:
                        lesson_json_name = lesson["title"]

                    if lesson_json_name == lesson_name:
                        for task in lesson["task_list"]:
                            if task['name'] == task_name:
                                for file in task["task_files"]:
                                    if file == file_name:
                                        i = 0

                                        # depending on what the key is in json
                                        if "task_windows" in task["task_files"][file]:
                                            placeholders_path = task["task_files"][file]['task_windows']
                                        else:
                                            placeholders_path = task["task_files"][file]['placeholders']

                                        for placeholders in placeholders_path:
                                            tag_name = self.opened_filename + "_" + str(i)
                                            line = placeholders['line']
                                            length = placeholders['length']
                                            start = placeholders['start']

                                            self.insert_placeholders(path, line, length, start, tag_name, done)

                                            hint = placeholders['hint']

                                            text = ""

                                            if str(hint).endswith("docs"):
                                                if (os.path.isdir(self.path + "/hints")):
                                                    with open(self.path + "/hints/" + hint, encoding="utf-8")as file:
                                                        text = file.read()
                                            else:
                                                text = hint

                                            self.hint_list = self.hint_list + str(i + 1) + ". " + text + '\n'
                                            i = i + 1

                                        break
                                break
                        break

    def insert_placeholders(self, path, line, length, start, tag_name, done):
        start_point = str(int(line) + 1) + "." + str(int(start))
        end_point = str(int(line) + 1) + "." + str(int(start) + int(length))
        get_workbench().get_current_editor().get_text_widget().tag_add(tag_name, start_point, end_point)
        if done:
            get_workbench().get_current_editor().get_text_widget().tag_config(tag_name,
                                                                              relief='sunken',
                                                                              borderwidth=1,
                                                                              background='green yellow')
        else:
            get_workbench().get_current_editor().get_text_widget().tag_config(tag_name,
                                                                              relief='sunken',
                                                                              borderwidth=1,
                                                                              background='yellow')

    def save_placeholders(self, event):
        # finds what file is opened at the moment
        file_path = ""
        editor = get_workbench().get_editor_notebook().get_current_editor()
        if editor:
            if (editor.get_filename()):
                file_path = editor.get_filename()

        # finds the item in the treeview to get the id
        for course in self.get_children():
            for lesson in self.get_children(course):
                for task in self.get_children(lesson):
                    for file_id in self.get_children(task):
                        if file_path == self.item(file_id)['values'][0]:
                            self.focus(file_id)
                            break

        # gets data
        node_id = self.focus()
        lesson_name = self.item(node_id)['values'][1]
        task_name = self.item(node_id)['values'][2]
        file_name = self.item(node_id)['values'][3]

        if (self.path):
            with open(self.path + '/course.json', 'r+', encoding="utf-8") as data_file:
                data = json.load(data_file, encoding="utf-8")
            for lesson in data["lessons"]:

                # depending on what the key is in json
                if "name" in lesson:
                    lesson_json_name = lesson["name"]
                else:
                    lesson_json_name = lesson["title"]

                if lesson_json_name == lesson_name:
                    for task in lesson["task_list"]:
                        if task['name'] == task_name:
                            for file in task["task_files"]:
                                if file == file_name:
                                    i = 0

                                    # depending on what the key is in json
                                    if "task_windows" in task["task_files"][file]:
                                        placeholders_path = task["task_files"][file]['task_windows']
                                    else:
                                        placeholders_path = task["task_files"][file]['placeholders']

                                    for placeholders in placeholders_path:
                                        tag_name = self.opened_filename + "_" + str(i)
                                        tags = get_workbench().get_current_editor().get_text_widget().tag_ranges(
                                            tag_name)

                                        start_numbers = str(tags[0]).split('.')
                                        final_numbers = str(tags[1]).split('.')

                                        line = int(start_numbers[0]) - 1
                                        length = int(final_numbers[1]) - int(start_numbers[1])

                                        placeholders['line'] = str(line)
                                        placeholders['length'] = str(length)
                                        placeholders['start'] = start_numbers[1]
                                    break
                            break
                    break

            # saves new placeholder parameters to course.json
            with open(self.path + '/course.json', 'w', encoding="utf-8") as new_data_file:
                json.dump(data, new_data_file)

    def check_if_task_correct(self, lines):
        if lines:
            done = True
            # checks if any test failed
            for line in lines:
                if "FAILED" in line:
                    done = False

            if done:
                # gets data
                node_id = self.focus()
                lesson_name = self.item(node_id)['values'][1]
                task_name = self.item(node_id)['values'][2]
                file_name = self.item(node_id)['values'][3]

                # changes it to true
                if (self.path):
                    tasks_done_file = open(self.path + "/tasks_done", "r", encoding="utf-8")
                    tasks_done_text = tasks_done_file.readlines()
                    tasks_done_file.close()
                    i = 0

                    # finds the right line in text
                    for line in tasks_done_text:
                        if line.startswith(lesson_name + "_" + task_name):
                            tasks_done_text[i] = lesson_name + "_" + task_name + "_true\n"
                        else:
                            i = i + 1

                    new_text = ""
                    for line in tasks_done_text:
                        new_text = new_text + line

                    # saves the file
                    tasks_done_file = open(self.path + "/tasks_done", "w", encoding="utf-8")
                    tasks_done_file.write(new_text)
                    tasks_done_file.close()

                    # add it to done_task_list
                    done_task = lesson_name + "_" + task_name
                    if done_task not in self.done_task_list:
                        self.done_task_list.append(done_task + '\n')

                    # change the placeholders to green
                    for tag in get_workbench().get_current_editor().get_text_widget().tag_names():
                        if str(tag).startswith(self.opened_filename):
                            get_workbench().get_current_editor().get_text_widget().tag_config(tag,
                                                                                              background='green yellow')

                    # add done to task
                    parent_id = self.parent(node_id)
                    self.item(parent_id, values="true")

                get_workbench().get_view("CourseFrame").fill_progress(self.all_task, self.done_task_list)
                self.refresh_tree_done()


# Course task folders
class Task_Browser(TaskFileBrowser):
    def __init__(self, master):
        TaskFileBrowser.__init__(self, master)


# shows Task Description
class TaskDescriptionFrame(tkinterhtml.HtmlFrame):
    def __init__(self, master):
        tkinterhtml.HtmlFrame.__init__(self, master,
                                       borderwidth=1,
                                       relief=tk.FLAT,
                                       horizontal_scrollbar="auto")

        self._update_frame_contents()
        get_workbench().get_editor_notebook().bind("<<NotebookTabChanged>>", self._update_frame_contents, True)

    def _update_frame_contents(self, event=None):
        editor = get_workbench().get_editor_notebook().get_current_editor()
        if editor:
            if (editor.get_filename()):
                path = os.path.dirname(editor.get_filename())
            else:
                path = None
        else:
            path = get_workbench().get_option("file.last_browser_folder")

        if path:
            if (os.path.isfile(path + "/task.html")):
                with open(path + "/task.html", 'r', encoding="UTF-8") as file:
                    self.set_content(file.read())
        else:
            self.set_content("<p></p>")


# shows Course Progress
class CourseFrame(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master, bg=CALM_WHITE)

        self.config(bd=30)
        self.canvas = tk.Canvas(self, bg='white', width=200, height=40)
        self.canvas.pack()
        self.text = tk.Label(self, bd=10, text="", bg=CALM_WHITE)
        self.text.pack()

    def fill_progress(self, all_tasks, done_task_list):
        self.canvas.delete("all")
        width = round(len(done_task_list) * 200 / all_tasks)
        self.canvas.create_rectangle(0, 0, width, 40, fill="yellow green")
        new_text = str(len(done_task_list)) + " of " + str(all_tasks) + " tasks completed"
        self.text.config(text=new_text)


class SelectCourseFrame(tk.Toplevel):
    def __init__(self, master, course_list):
        tk.Toplevel.__init__(self, master)

        self.config(bd=10)

        heading_font = font.nametofont("TkHeadingFont").copy()
        heading_font.configure(size=12, weight="bold")
        self.heading_label = tk.Label(self, text="Select an existing course...",
                                      font=heading_font, bd=10)
        self.heading_label.pack()

        self.listbox = tk.Listbox(self, activestyle="dotbox", height=len(course_list))
        self.listbox.pack(fill="both")

        for course in course_list:
            self.listbox.insert("end", course)

        self.empty_place = tk.Label(self)
        self.empty_place.pack()

        self.select_button = tk.Button(self, text="Open", command=self._select, default="active")
        self.select_button.pack()

        tk.mainloop()

    def _select(self):
        selection = self.listbox.curselection()
        if selection != ():
            course_name = self.listbox.get(selection)
            course_path = os.path.expanduser(os.path.join("~", ".thonny", "interactive_courses", course_name))
            get_workbench().show_view("Task_Browser")
            get_workbench().show_view("TaskDescriptionFrame")
            get_workbench().show_view("CourseFrame")
            get_workbench().get_view("Task_Browser").open_course(course_path, course_name)
            get_workbench().set_option("interactive.last_course_folder", course_path)
            self.destroy()


class InteractiveDialog(tk.Toplevel):
    def __init__(self, master, reason, text):
        tk.Toplevel.__init__(self, master)

        self.geometry("+%d+%d" % (master.winfo_rootx() + master.winfo_width() // 2 - 50,
                                  master.winfo_rooty() + master.winfo_height() // 2 - 150))

        main_frame = ttk.Frame(self)
        main_frame.grid(sticky=tk.NSEW, ipadx=15, ipady=15)
        main_frame.rowconfigure(0, weight=1)
        main_frame.columnconfigure(0, weight=1)

        self.title("Interactive Dialog")

        self.resizable(height=tk.FALSE, width=tk.FALSE)
        self.transient(master)
        self.grab_set()
        self.protocol("WM_DELETE_WINDOW", self._ok)

        heading_font = font.nametofont("TkHeadingFont").copy()
        heading_font.configure(size=14, weight="bold")
        heading_label = ttk.Label(main_frame,
                                  text=reason,
                                  font=heading_font)
        heading_label.grid()

        text_font = font.nametofont("TkDefaultFont").copy()
        text_font.configure(size=12)
        text_label = ttk.Label(main_frame,
                               text=text,
                               justify=tk.LEFT, font=text_font)
        text_label.grid()

        ok_button = ttk.Button(main_frame, text="OK", command=self._ok, default="active")
        ok_button.grid(pady=(0, 15))
        ok_button.focus_set()

        self.bind('<Return>', self._ok, True)
        self.bind('<Escape>', self._ok, True)
        self.wait_window()

    def _ok(self, event=None):
        self.destroy()


# sends Shell a command that run tests
def run_test_menu_command():
    get_runner().execute_current(COMMAND_NAME, True)


# TODO return False
def run_test_menu_available():
    return True


def handle_run_test_from_shell(cmd_line):
    _, script_filename = cmd_line.strip().split(maxsplit=1)
    script_dir = get_runner().get_cwd()
    test_helper_dir = os.path.abspath(os.path.join(script_dir, "..", ".."))

    course_name = get_workbench().get_view("Task_Browser").course_name
    initial_project_dir = os.path.expanduser(os.path.join("~", ".thonny",
                                                          "interactive_courses_backup",
                                                          course_name))

    if not os.path.isabs(script_filename):
        script_filename = os.path.join(script_dir, script_filename)

    test_filename = "tests.py"

    get_runner().send_command(ToplevelCommand(command="Run",
                                              filename=test_filename,
                                              args=[initial_project_dir,
                                                    script_filename],
                                              environment={"PYTHONPATH": test_helper_dir}))


def clean_up_shell(event):
    text = get_workbench().get_view("ShellView").text
    lines = text.get("1.0", "end").splitlines()
    educational_lines = []
    failed_text = ""
    failed_nr = 1
    for i in range(len(lines) - 1, 0, -1):
        line = lines[i]
        if line.startswith("#educational_plugin "):
            educational_lines.append(line)
            line_nr = i + 1
            if "FAILED" in line:
                failed_text = failed_text + str(failed_nr) + ". " + str(line).split("+")[1].strip() + '\n'
                failed_nr = failed_nr + 1
            text.direct_delete(str(line_nr) + ".0", str(line_nr) + ".end+1c")
    get_workbench().get_view("Task_Browser").check_if_task_correct(educational_lines)

    if failed_text != "":
        InteractiveDialog(get_workbench(), "Interactive Test Error", failed_text)


# for getting full path of png's
def get_full_path(filename):
    module_dir = os.path.dirname(sys.modules[__name__].__file__)
    return os.path.join(module_dir, filename)


def load_plugin():
    get_workbench().add_option("interactive.last_course_folder", "")
    get_workbench().add_view(Task_Browser, "Course", "nw")
    get_workbench().add_view(TaskDescriptionFrame, "Task Description", "ne")
    get_workbench().add_view(CourseFrame, "Progress", "sw")
    Interactive()

    get_workbench().get_view("ShellView").add_command(COMMAND_NAME, handle_run_test_from_shell)
    get_workbench().bind("ToplevelResult", clean_up_shell, True)
